import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './view/App'
import Index from './pages/index'
import Login from './pages/login'
// import Register from './pages/register'
import Home from './pages/home'
import Pending from './orders/pending'
import Inprogress from './orders/inprogress'
import Onhold from './orders/onhold'
import Revised from './orders/revised'
import Completed from './orders/completed'
import Detail from './orders/view'
import Refunds from './orders/refunds'
import Ordering from './orders/ordering'
import Confirm from './orders/confirm'
import Payment from './orders/payment'
import AddPayment from './orders/addpyament'
import Chat from './orders/chat'

import Forget from './pages/forget_password'
import Confirm_Pass from './pages/confirm'
import Reset_Pass from './pages/reset_password'
import Profile from './pages/profile'
// import Edit from './orders/edit'
// import Refer from './pages/refer'
// import Services from './pages/services'
import Referals from './pages/referals'







const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index,
        },
        // {
        //     path: '/refer',
        //     name: 'refer',
        //     component: Refer
        // },
        {
            path: '/referals',
            name: 'referals',
            component: Referals
        },
        // {
        //     path: '/services',
        //     name: 'services',
        //     component: Services
        // },
        // {
        //     path: '/about',
        //     name: 'about',
        //     component: About
        // },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        // {
        //     path: '/register',
        //     name: 'register',
        //     component: Register
        // },
        //
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/pending',
            name: 'pending',
            component: Pending
        },
        {
            path: '/inprogress',
            name: 'inprogress',
            component: Inprogress
        },
        {
            path: '/onhold',
            name: 'onhold',
            component: Onhold
        },
        {
            path: '/revised',
            name: 'revised',
            component: Revised
        },
        {
            path: '/completed',
            name: 'completed',
            component: Completed
        },
        {
            path: '/detail/:id',
            name: 'detail',
            component: Detail
        },
        {
            path: '/refunds',
            name: 'refunds',
            component: Refunds
        },
        {
            path: '/ordering',
            name: 'ordering',
            component: Ordering
        },
        {
            path: '/confirm/:id',
            name: 'confirm',
            component: Confirm
        },
        {
            path: '/payment/:id',
            name: 'payment',
            component: Payment
        },
        // {
        //     path: '/edit/:id',
        //     name: 'edit',
        //     component: Edit
        // },
        {
            path: '/addpayment/:id',
            name: 'addpayment',
            component: AddPayment
        },
        {
            path: '/chat/:id',
            name: 'chat',
            component: Chat
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget
        },
        {
            path: '/confirm_pass',
            name: 'confirm_pass',
            component: Confirm_Pass
        },
        {
            path: '/reset_pass',
            name: 'reset_pass',
            component: Reset_Pass
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

