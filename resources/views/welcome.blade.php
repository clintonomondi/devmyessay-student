<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Devmyessay</title>
    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Developing High Quality Essay"/>
    <meta property="og:image" content="/pic/logo.jpeg"/>
    <meta property="og:description" content="DevMyEssay has writers and researchers from all academic fields who work diligently in order to meet the requirements of orders."/>

    <!-- Favicons -->
    {{--<link href="assets/img/favicon.png" rel="icon">--}}
    {{--<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">--}}

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.0/css/font-awesome.css">--}}
    <!-- Template Main CSS File -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/css/card.css" rel="stylesheet">
    <link href="/css/sidebar.css" rel="stylesheet">
    <link href="/css/chat.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/font-awesome.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;1,100;1,200&display=swap" rel="stylesheet">

    <script data-ad-client="ca-pub-2885740322818519" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
<div class="content2">
<div id="app">
    <app></app>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</div>

{{--<div class="container">--}}
    {{--<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
    {{--<!-- devmyessay -->--}}
    {{--<ins class="adsbygoogle"--}}
         {{--style="display:block"--}}
         {{--data-ad-client="ca-pub-2885740322818519"--}}
         {{--data-ad-slot="3782448631"--}}
         {{--data-ad-format="auto"--}}
         {{--data-full-width-responsive="true"></ins>--}}
    {{--<script>--}}
        {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
    {{--</script>--}}
{{--</div>--}}

<footer id="footer" class="mb-0">
    {{--<div class="footer-newsletter">--}}
        {{--<div class="container">--}}
            {{--<div class="row justify-content-center">--}}
                {{--<div class="col-lg-6">--}}
                    {{--<h4>Join Our Newsletter</h4>--}}
                    {{--<p>Enter email address</p>--}}
                    {{--<form action="" method="post">--}}
                        {{--<input type="email" name="email"><input type="submit" value="Subscribe">--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>DevMyEssay</span></strong>. All Rights Reserved
        </div>
        <div class="row">
            <div class="col-sm-3">
                <a href="#">Call us: +1 (925) 4056454 </a>
            </div>
            <div class="col-sm-3">
                <a href="#">Email: info@devmyessay.com </a>
            </div>
            <div class="col-sm-3">
                <a href="/Privacy Policy.pdf">Privacy policy </a>
            </div>

        </div>
        <div class="credits">
            <div class="footer-social">
                <div class="payment-cart">
                    <img src="/assets/img/1.png" alt="">
                </div>
            </div>
        </div>
    </div>
</footer><!-- End Footer -->



{{--<div id="preloader"></div>--}}
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"  ></script>


{{--<!-- Vendor JS Files -->--}}
<script src="/assets/vendor/aos/aos.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="/assets/vendor/php-email-form/validate.js"></script>
<script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="/assets/vendor/waypoints/noframework.waypoints.js"></script>


<!-- Template Main JS File -->
<script src="/assets/js/main.js"></script>
<script src="/loader/center-loader.js"></script>
<script src="/js/common.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.min.js" integrity="sha512-RGbSeD/jDcZBWNsI1VCvdjcDULuSfWTtIva2ek5FtteXeSjLfXac4kqkDRHVGf1TwsXCAqPTF7/EYITD0/CTqw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.esm.js" integrity="sha512-rX92e1gJcy6G+ivRwDY5NnrDdGz37qBHqhNgDB9b9oT83N+vcKOs7GCcDTvKz/mFanYSTz+EoRi8SGlMOd3MoQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.js" integrity="sha512-LlFvdZpYhQdASf4aZfSpmyHD6+waYVfJRwfJrBgki7/Uh+TXMLFYcKMRim65+o3lFsfk20vrK9sJDute7BUAUw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/helpers.esm.js" integrity="sha512-334wuK4vkaONVysNjemyZ0HZDlNvZnjAEvrOu4LFn4fCrCfzWJjFG7wePPfZtW7bGmWvjN/r0RkwiaQ67+hsNg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/helpers.esm.min.js" integrity="sha512-5XGk12SGIo2btywdra6Pg6M+mHRnso/oZE4TVQ9+FAqHxG2dN8FHk4/fw+XXd9xUFSd0xatlMjEEBbwFgq7THw==" crossorigin="anonymous"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
<script src="{{asset('js/chat.js')}}"></script>

</body>

</html>
